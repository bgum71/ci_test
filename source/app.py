from sanic import Sanic, response


app = Sanic(__name__)

@app.route('/')
async def hi(request):
    hello = '\
            <h>Hello world</h>\
            '
    return response.html(hello)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
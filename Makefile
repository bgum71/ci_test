SSH_KEY_PATH ?= .ssh/gitlab_bgum
IMAGE_NAME ?=  "ci_test"
BASE_GITLAB_URL ?= "git+ssh://git@gitlab.com:bgum71/"
ADDITIONAL_DOCKER_BUILD_ARGS ?=

# Version is a git tag name for current SHA commit hash or this hash if tag is not presented
APP_VERSION ?= $$(git describe --exact-match --tags $(git log -n1 --pretty='%h') 2> /dev/null || \
				  git log -n1 --pretty='commit_sha:%h')

vars:
	@echo IMAGE_NAME=${IMAGE_NAME}
	@echo SSH_KEY_PATH=${SSH_KEY_PATH}
	@echo BASE_GITLAB_URL=${BASE_GITLAB_URL}

image:
	docker build -f build/Dockerfile -t ${IMAGE_NAME} . --force-rm=True \
								 --build-arg rsakey="$$(cat ${SSH_KEY_PATH})" \
								 --build-arg BASE_GITLAB_URL=${BASE_GITLAB_URL} \
								 --build-arg APP_VERSION=${APP_VERSION} \
								 ${ADDITIONAL_DOCKER_BUILD_ARGS}

run:
	docker-compose -f build/docker-compose.yml up web

clean:
	docker-compose -f build/docker-compose.yml down
